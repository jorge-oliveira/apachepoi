
import org.apache.poi.ss.usermodel.*;

import java.io.*;

public class ReadingDataV4 {

    public static void main(String[] args) throws IOException {

        try (InputStream inp = new FileInputStream("src/ExcelData/new1.xlsx")) {
            //InputStream inp = new FileInputStream("workbook.xlsx");

            Workbook wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            Sheet sheetName = wb.getSheet("Folha1");
            Row row = sheet.getRow(2);
            Cell cell = row.getCell(3);

            System.out.println(cell.getStringCellValue());
            //System.out.println(cell.getStringCellValue());


            if (cell == null)
                cell = row.createCell(3);
            cell.setCellType(CellType.STRING);
            cell.setCellValue("a test");

            // Write the output to a file
            try (OutputStream fileOut = new FileOutputStream("src/ExcelData/new2.xls")) {
                wb.write(fileOut);
            }
        }
    }
}
