import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ReadingExcel {

    public static void main(String[] args) throws IOException, InvalidFormatException {

        // input file stream
        File f = new File("src/ExcelData/new.xlsx");

        // connection to input stream
        FileInputStream fi = new FileInputStream(f);

        // connecting workbook to input stream
        Workbook workbook = new WorkbookFactory().create(fi);

        // get the first sheet example with complete the path to the class
        org.apache.poi.ss.usermodel.Sheet sheet0 = workbook.getSheetAt(0);
        // get data from sheet 1
        Sheet sheet1 = workbook.getSheetAt(1);

        // get the first row of sheet 0
        Row row0 = sheet0.getRow(0);

        // get the first cell
        Cell cell0 = row0.getCell(0);
        Cell cell1 = row0.getCell(1);
        Cell cell2 = row0.getCell(2);
        Cell cell3 = row0.getCell(3);

        System.out.println("Reading data from sheet 1");
        System.out.println("cell 0 is --> " + cell0 + " Cell 1 is --> " + cell1);
/*
        System.out.println("\nReading data from sheet 2");
        // using a for loop for reading the data
        for (Row row : sheet1) {

            for (Cell cell : row) {
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING:
                        System.out.println(cell.getStringCellValue() + "\t");
                        break;
                    case Cell.CELL_TYPE_BLANK:
                        System.out.println("Blank Cell" + "\t");
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        System.out.println(cell.getNumericCellValue() + "\t");
                        break;

                }// end switch
            }// end innner for
        } // end outer for
*/

    }
}
