import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class WritingExcel {

    public static <duble> void main(String[] args) throws IOException {

        // creating work book
        XSSFWorkbook workbook = new XSSFWorkbook();

        // creating sheet on the workbook, the XSSFSheet have private constructor
        XSSFSheet sheet1 = workbook.createSheet("first sheet");
        XSSFSheet sheet2 = workbook.createSheet("second sheet");

        // create row in sheet1
        Row row0 = sheet1.createRow(0);
        Row row1 = sheet1.createRow(1);
        Row row2 = sheet1.createRow(2);

        // create cells in row 0
        Cell cellA = row0.createCell(0);
        // repeating above step for other cells
        Cell cellB = row0.createCell(1);
        Cell cellC = row0.createCell(2);
        Cell cellD = row0.createCell(3);

        // setting cell value
        cellA.setCellValue("Name");
        cellB.setCellValue("email");
        cellC.setCellValue("mobile Number");

        // creating data with a for loop on the second sheet
        for(int rows=0; rows<10; rows++){

            Row row = sheet2.createRow(rows);
            for (int cols=0; cols < 10; cols++){
                Cell cells = row.createCell(cols);
                cells.setCellValue((int)(Math.random()*100));
            }
        }

        // creating file stream
        //File f = new File("d:/new.xlsx");
        File f = new File("src/ExcelData/new.xlsx");

        // chaining output stream to path
        FileOutputStream fo = new FileOutputStream(f);

        // writing workbook to outputstream
        workbook.write(fo);

        // closing stream
        fo.close();
        System.out.println("Excel file is written");

    }
}
